# nimiq-miner
Miner for Nimiq Release Candidate

[Check here for CPU mining hashrate benchmarks](https://docs.google.com/spreadsheets/d/1RjU6aQnRcXj6RQOGkG8DAscFjWyV1axdSECJbWy0c7c/edit#gid=0)

### Requirements
* Node 8.9.0+ LTS (Carbon)

### Installation
```
git clone <repo>
yarn
npm run mine
```

### Running the Miner with configs
`npm run start -- [--address <items>] --network test`

**Wallet Address Format**

`NQ43-GQ0B-R7AJ-5SUG-Q2HC-3XMP-MNRU-8VM0-AJEG` **not** `NQ43 GQ0B R7AJ 5SUG Q2HC 3XMP MNRU 8VM0 AJEG`


 **Options:**

  * `-V, --version`          
    * output the version number
  * `--address <items>`      
    * public wallet address (default: new wallet will be created)
  * `-t, --threads [count]`  
    * Number of miner threads (default: `$NIMIQ_MINER_THREADS` or `auto`)
  * `-n, --network [value]`  
    * Network to mine (default: `$NIMIQ_NETWORK` or `test`)
  * `--keenProject [hex]`   
    * [Keen IO](keen.io) Project ID')
  * `--keenKey [hex]`       
    * [Keen IO](keen.io) Project Key')
  * `-h, --help`             
    * output usage information
