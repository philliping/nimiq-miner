const os = require('os')
const si = require('systeminformation');
const program = require('commander')
const moment = require('moment')
const winston = require('winston')
const Nimiq = require('../nimiq/dist/node.js')
const KeenTracking = require('keen-tracking');
const uuid = require('uuid/v4')
const config = require('dotenv-extended').load({
  path: '../.env'
})

process.env.UV_THREADPOOL_SIZE = Math.ceil(Math.max(4, os.cpus().length * 2));


const WALLET_SEED_REGEX = /^[0-9a-f]{128}$/i
const DISPLAY_INTERVAL = 360 * 1000

const MEGAHASH = 1000 * 1000
const KILOHASH = 1000

const ONE_MINUTE = 60
const ONE_HOUR = 60 * ONE_MINUTE
const ONE_DAY = 24 * ONE_HOUR
const ONE_WEEK = 7 * ONE_DAY
const ONE_MONTH = 30 * ONE_DAY
const ONE_YEAR = 365 * ONE_DAY

function humanizeHashrate (rate, decimalPlaces = 2) {
  if (rate >= MEGAHASH) {
    return `${(rate / MEGAHASH).toFixed(decimalPlaces)} MH/s`
  } else if (rate >= KILOHASH) {
    return `${(rate / KILOHASH).toFixed(decimalPlaces)} kH/s`
  }
  return `${rate.toFixed(decimalPlaces)} H/s`
}

function humanizeExpectedTime (seconds, decimalPlaces = 1) {
  if (seconds >= ONE_YEAR) {
    return `${(seconds / ONE_YEAR).toFixed(decimalPlaces)} years`
  } else if (seconds >= ONE_MONTH) {
    return `${(seconds / ONE_MONTH).toFixed(decimalPlaces)} months`
  } else if (seconds >= ONE_WEEK) {
    return `${(seconds / ONE_WEEK).toFixed(decimalPlaces)} weeks`
  } else if (seconds >= ONE_DAY) {
    return `${(seconds / ONE_DAY).toFixed(decimalPlaces)} days`
  } else if (seconds >= ONE_HOUR) {
    return `${(seconds / ONE_HOUR).toFixed(decimalPlaces)} hours`
  } else if (seconds >= ONE_MINUTE) {
    return `${Math.ceil(seconds / ONE_MINUTE)} minutes`
  }
  return `${seconds} seconds`
}

let client
async function main (args, logger) {

  if (config.KEEN_PROJECT && config.KEEN_KEY) {
    client = new KeenTracking({
        projectId: config.KEEN_PROJECT,
        writeKey: config.KEEN_KEY
    });
  }

  // Validate the wallet seed (if it was provided OR in env)
  const walletAddress = config.NIMIQ_WALLET
  const net = config.NIMIQ_NETWORK
  const project = config.PROJECT
  const slug = config.SLUG

  // Set Nimiq Log level
  Nimiq.Log.instance.level = Nimiq.Log.Level.ERROR

  // Configure network
  Nimiq.GenesisConfig.init(Nimiq.GenesisConfig.CONFIGS[net]);
  const networkConfig = new Nimiq.DumbNetworkConfig()
  const consensus = await Nimiq.Consensus.light(networkConfig)
  const { blockchain, mempool, network } = consensus
  const { accounts } = blockchain

  // Initialize wallet
  logger.info('Initializing wallet...')

  var walletStore = await new Nimiq.WalletStore()
  var wallet = {}
  if (walletAddress) {
    const address = Nimiq.Address.fromUserFriendlyAddress(walletAddress)
    wallet = {address: address}
    const w = await walletStore.get(address)
    if (w) {
      wallet = w
      await walletStore.setDefault(wallet.address)
    }
  } else {
    wallet = await walletStore.getDefault()
  }
  const account = await accounts.get(wallet.address)

  logger.info(`Wallet address: ${wallet.address.toUserFriendlyAddress(true)}`)
  logger.info(`Balance: ${Nimiq.Policy.satoshisToCoins(account.balance)} NIM`)

  // Create a multi-threaded miner (auto = 1 per CPU)
  let lastMinedBlock = null
  let lastMinedTimestamp = null

  const extraData = Nimiq.BufferUtils.fromAscii(uuid())
  const miner = new Nimiq.Miner(blockchain, accounts, mempool, network.time, wallet.address, extraData)

  miner.threads = os.cpus().length > 4 ? Math.max(1, os.cpus().length) - 1 : Math.max(1, os.cpus().length)

  // Listen for blockchain changes
  blockchain.on('head-changed', (head) => {
    if (consensus.established) {
      logger.info(`Now on block ${head.height}`)
    }
  })

  // Listen for miner events
  miner.on('start', () => logger.info(`Mining has started on ${miner.threads} thread(s).`))
  miner.on('stop', () => {
    logger.info('Mining has stopped.')
    if (client) {
      client.recordEvent('mining_stopped', {
        wallet: wallet.address.toUserFriendlyAddress(true),
        project: project,
        slug: os.hostname()
      });
    }
  })

  miner.on('error', () => {
    logger.info('Mining has stopped.')
    if (client) {
      client.recordEvent('error', {
        wallet: wallet.address.toUserFriendlyAddress(true),
        project: project,
        slug: os.hostname()
      });
    }
  })

  miner.on('block-mined', (block) => {
    logger.info(`Mined block ${block.height}! nonce=${block.nonce}`)
    lastMinedTimestamp = moment.utc()
    lastMinedBlock = block

    if (client) {
      const balanceValue = Nimiq.Policy.satoshisToCoins(account.balance)
      client.recordEvent('block_mined', {
        wallet: wallet.address.toUserFriendlyAddress(true),
        mined_block: block.height,
        nonce: block.nonce,
        balance: balanceValue.toFixed(2),
        project: project,
        slug: os.hostname()
      });
    }
  })

  // Listen for consensus changes
  consensus.on('syncing', () => logger.info('Syncing with network...', blockchain.targetHeight))

  // Start miner on consensus established
  consensus.on('established', () => {
    logger.info('Consensus established.')
    miner.startWork()
    if (client) {
      client.recordEvent('consensus_established', {
        wallet: wallet.address.toUserFriendlyAddress(true),
        project: project,
        slug: os.hostname()
      });
    }
  })

  // Stop miner on consensus lost
  consensus.on('lost', () => {
    logger.warn('Consensus lost.')
    miner.stopWork()
    if (client) {
      client.recordEvent('consensus_lost', {
        wallet: wallet.address.toUserFriendlyAddress(true),
        project: project,
        slug: os.hostname()
      });
    }
  })

  // Display hashrate every few seconds
  setInterval(async () => {
    if (miner.working) {
      const balanceValue = Nimiq.Policy.satoshisToCoins(account.balance)

      const nBits = blockchain.head.header.nBits
      const difficulty = Nimiq.BlockUtils.compactToDifficulty(nBits)
      const globalHashrate = difficulty * Math.pow(2, 16) / Nimiq.Policy.BLOCK_TIME

      const myWinProbability = miner.hashrate / globalHashrate
      const expectedHashSeconds = (1 / myWinProbability) * Nimiq.Policy.BLOCK_TIME

      const processes = await si.processLoad("node")
      const cpuData = await si.cpu()

      logger.info(`Block ${blockchain.head.height} | Global Hashrate: ${humanizeHashrate(globalHashrate)} | Hashrate: ${humanizeHashrate(miner.hashrate)} | Balance: ${balanceValue.toFixed(2)} | Expected: every ${humanizeExpectedTime(expectedHashSeconds)}`)
      if (client) {
        client.recordEvent('mining', {
          wallet: wallet.address.toUserFriendlyAddress(true),
          block: blockchain.head.height,
          global_hashrate: humanizeHashrate(globalHashrate),
          hashrate: humanizeHashrate(miner.hashrate),
          expected: humanizeExpectedTime(expectedHashSeconds),
          balance: balanceValue.toFixed(2),
          threads: miner.threads,
          raw_hashrate: miner.hashrate,
          raw_global_hashrate: globalHashrate,
          project: project,
          slug: os.hostname(),
          cpu: cpuData,
          usage: processes
        });
      }
    }
  }, DISPLAY_INTERVAL)

  // Start connecting to Nimiq network
  logger.info('Connecting to Nimiq network...')
  network.connect()
}

// Parse command line arguments
program.version('0.1.0')
  .description('Nimiq Release Candidate Miner')
  .option('--address <items>', 'public wallet address', process.env.NIMIQ_WALLET)
  .option('-t, --threads [count]', 'Number of miner threads', process.env.NIMIQ_MINER_THREADS)
  .option('-n, --network [value]', 'Network type', process.env.NIMIQ_NETWORK)
  .option('--keenProject [hex]', 'keen.io project id', process.env.KEEN_PROJECT)
  .option('--keenKey [hex]', 'keen.io key', process.env.KEEN_KEY)
  .option('--project [project]', 'project identifier', process.env.PROJECT)
  .option('--slug [slug]', 'slug', process.env.SLUG)
  .parse(process.argv)

// Initialize logger
const logger = new winston.Logger()
  .add(winston.transports.Console, {
    label: 'miner',
    level: process.NODE_ENV === 'production' ? 'info' : 'debug',
    timestamp: () => `[${moment().format('HH:mm:ss.SSS')}]`,
    colorize: true,
    json: false
  })

// Start main program (with catch-all)
main(program, logger)
  .catch(err => {
    logger.error(err)
    process.exit(1)
  })
